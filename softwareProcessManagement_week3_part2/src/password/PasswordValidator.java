package password;

public class PasswordValidator {

	public static boolean checkPasswordLength(String password) {
//		// before
//		
//		if (password.length() >= 8) {
//			return true;
//		}
//		else {
//			return false;
//		}
		
		// after
		
		return password.length() > 7;

	}
	
	public static boolean checkPasswordDigits(String password) {
		
		// before
//		int digits = 0;
//		char[] passwordArr = password.toCharArray();
//		for (int i = 0; i < passwordArr.length; i++) {
//			if (passwordArr[i] > '0' && passwordArr[i] <= '9') {
//				digits++;
//			}
//		}
//		return digits == 2;
		
		// after
		
		int digits = 0, i = 0;
		char[] passwordArr = password.toCharArray();
		do {
			if (Character.isDigit(passwordArr[i])) {
				digits++;
			}
			i++;
			
		} while (digits != 2 && i < passwordArr.length);
		
		return digits == 2;
		
		
	}
}
