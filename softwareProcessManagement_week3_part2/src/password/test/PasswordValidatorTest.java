package password.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import password.PasswordValidator;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/*
	 * Requirement 1: Password must have at least 8 characters
	 */
	
	// *******************
	// Acceptance test
	// *******************
	@Test
	public void testCheckPasswordLength() {
		
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("12345678"));
	}
	
	// *******************
	// Exception test
	// *******************
	@Test
	public void testCheckPasswordLengthException() {
		
		assertFalse("Valid password length", PasswordValidator.checkPasswordLength("1"));
	}
	
	// *******************
	// Boundary test - In
	// *******************
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		
		assertTrue("Invalid password length", PasswordValidator.checkPasswordLength("123456789"));
	}
	
	// *******************
	// Boundary test - Out
	// *******************
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		
		assertFalse("Valid password length", PasswordValidator.checkPasswordLength("1234567"));
	}
	
	/*
	 * Requirement 2: Password must contain at least two digits
	 */
	
	// *******************
	// Acceptance test
	// *******************
	@Test
	public void testCheckPasswordDigits() {
		
		assertTrue("Your password does not contain at least two numbers", 
				PasswordValidator.checkPasswordDigits("q12345wer"));
	}
	
	// *******************
	// Exception test
	// *******************
	@Test
	public void testCheckPasswordDigitsException() {
		assertFalse("Your password has enough digits", PasswordValidator.checkPasswordDigits("qwerty"));
	}
	
	// *******************
	// Boundary test - In
	// *******************
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		
		assertTrue("Your password requires at least two numbers", 
				PasswordValidator.checkPasswordDigits("q123tyuui"));
	}
	
	// *******************
	// Boundary test - Out
	// *******************
	@Test
	public void testCheckPasswordDigitBoundaryOut() {
		
		assertFalse("Your password has enough digits", PasswordValidator.checkPasswordDigits("q1wertyuop"));
	}
}
